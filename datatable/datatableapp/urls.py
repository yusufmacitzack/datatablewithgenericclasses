from django.urls import path, include

from datatableapp import views


urlpatterns = [
       path('list/<int:pk>/', views.MemberDetail.as_view(), name='detailview'),
       path('list/', views.MemberList.as_view(), name='memberlist'),
]
