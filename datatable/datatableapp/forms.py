from django.forms import ModelForm
from .models import *

class DetailForm(ModelForm):
    class Meta:
        model = Member
        fields = ('name', 'surname')