# Create your views here.
from django.shortcuts import render, redirect, reverse
from .models import Member
from django.views.generic import DetailView, ListView
from .forms import *
from django.views.generic.edit import FormMixin
from django.views.generic.edit import ModelFormMixin
# Create your views here.

class MemberList(ListView):

    queryset = Member.objects.all()
    template_name = "index.html"


class MemberDetail(DetailView, ModelFormMixin):
    
    template_name = "detail.html"
    model = Member
    form_class = DetailForm
    
    def post(self, request, *args, **kwargs):
        print('POSTLANDIIIIIII*****')
        self.object = self.get_object()
        form = self.get_form()
        if form.is_valid():
            return self.form_valid(form)
            
        else:
            return self.form_invalid(form)

    def form_valid(self, form):
        form.save()
        return super(MemberDetail, self).form_valid(form)

    def get_success_url(self):
        return reverse('memberlist')




    
